"""
This module is an example of a barebones QWidget plugin for napari

It implements the ``napari_experimental_provide_dock_widget`` hook specification.
see: https://napari.org/docs/dev/plugins/hook_specifications.html

Replace code below according to your needs.
"""
#from napari import Viewer
from magicgui import magic_factory
from magicgui.widgets import PushButton
import pathlib
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from .utils import get_anndata, ColormapChoices, add_spacem_images, load_json
import napari.types
from typing import List
from magicgui import magicgui
from napari import Viewer
import scanpy as sc


def _init(widget):
    
    widget.feature._default_choices = []
    widget.label_column._default_choices = []
    widget.adata_layer._default_choices = []
    
    def get_feature_choices(*args):
        try:
            if widget.sc_adata.value:
                if widget.adata_type.value == "cells":
                    active_adata =  widget.sc_adata.value
                else:
                    active_adata = widget.am_adata.value
            else:
                return [""]
                
            if widget.feature_type.value == "ion":
                return list(active_adata.var.index)
            else:
                return active_adata.obs.columns
        except IOError:
            return [""]
        
    def get_label_column_choices(*args):
        try:
            if widget.sc_adata.value:
                if widget.adata_type.value == "cells":
                    active_adata =  widget.sc_adata.value
                else:
                    active_adata = widget.am_adata.value
            else:
                return [""]
                
            return list(active_adata.obs.columns)
        except IOError:
            return [""]
    
    widget.feature._default_choices = get_feature_choices
    widget.label_column._default_choices = get_label_column_choices
    
    def get_adata_layers(*args):
        if widget.sc_adata.value:
            if widget.adata_type.value == "cells":
                active_adata =  widget.sc_adata.value
            else:
                active_adata = widget.am_adata.value
            print("set adata layers")
            print(list(active_adata.layers.keys()))
            return ["X", *list(active_adata.layers.keys())]
        else:
            return ["X"]
        
    widget.adata_layer._default_choices = get_adata_layers
        
 
 
    @widget.SpaceM_dir.changed.connect
    def open_spacem_dir(event):
        print("CHANGE DIR")
        
        # event value will be the new path
        # get_df will give you the cached df
        # ...reset_choices() calls the "get_feature_choices" function above
        # to keep them updated with the current dataframe            
        ds_name, cell_masks, cropped_premaldi, cropped_postmaldi, am_masks, overlap_masks = add_spacem_images(event.value, widget.viewer.value)
        widget.am_masks.value = am_masks
        widget.cell_masks.value = cell_masks
    
    def set_adata_properties(widget, adata):
        widget.adata_layer.reset_choices()
        # widget.adata_layer.choices = ["X", *adata.layers.keys()]
        # widget.feature.reset_choices()
        if widget.feature_type.value == "ion":
            widget.feature.reset_choices()
            # widget.feature.choices = list(adata.var.index)
        else:
            widget.feature.reset_choices()
            # widget.feature.choices = adata.obs.columns
            
        features = adata.obs.columns
        widget.label_column.reset_choices()
        # widget.label_column.choices = features
        
        if 'label' in features:
            widget.label_column.value = 'label'
        elif 'Label' in features:
            widget.label_column.value = 'Label'
        elif 'index' in features:
            widget.label_column.value = 'index'
        elif 'cell_id' in features:
            widget.label_column.value = 'cell_id' 
        elif 'cellid' in features:
            widget.label_column.value = 'cellid'
        if "obs_index" in features:
            widget.label_column.value = 'obs_index'

    @widget.am_adata_path.changed.connect
    def update_am_features(event):
        adata = sc.read_h5ad(event.value)
        adata.obs["obs_index"] = adata.obs.index
        widget.am_adata.value = adata
        
        print("Read AM adata:")
        print(widget.am_adata.value)
        if widget.adata_type.value == "AMs":
            set_adata_properties(widget, widget.am_adata.value)
                
    
    @widget.sc_adata_path.changed.connect
    def update_sc_features(event):
        adata = sc.read_h5ad(event.value)
        adata.obs["obs_index"] = adata.obs.index
        widget.sc_adata.value = adata
        print("Read sc adata:")
        print(widget.sc_adata.value)
        if widget.adata_type.value == "cells":
            set_adata_properties(widget, widget.sc_adata.value)       
            
    
    @widget.adata_type.changed.connect
    def update_adata_type(event):
        if event.value == "cells":
            set_adata_properties(widget, widget.sc_adata.value)
        else:
            set_adata_properties(widget, widget.am_adata.value)
         
    
    @widget.feature_type.changed.connect
    def update_feature_type(event):
        if widget.adata_type.value == "cells":
            active_adata =  widget.sc_adata.value
        else:
            active_adata = widget.am_adata.value
        print("CHANGED FEATURE TYPE", widget.feature.choices)    
        widget.feature.choices = [""]
        if event.value == "ion":
            widget.feature.reset_choices()
            # widget.feature.choices = list(active_adata.var.index)
        else:
            widget.feature.reset_choices()
            # widget.feature.choices = active_adata.obs.columns
        widget.feature.value = widget.feature.choices[0]
                
    
    @widget.feature.changed.connect
    def update_rescaling(event):
        print("VALUE CHANGED", event.value)
        
        if widget.adata_type.value == "cells":
            active_adata =  widget.sc_adata.value
        else:
            active_adata = widget.am_adata.value
            
            
        if event.value == '':
            print("widget feature", widget.feature)
            if event.value == "ion":
                widget.feature.reset_choices()
                # widget.feature.choices = list(active_adata.var.index)
            else:
                widget.feature.reset_choices()
                # widget.feature.choices = active_adata.obs.columns
            widget.feature.value = widget.feature.choices[0]
        
        else:
            if widget.feature_type.value == "ion":
                if widget.adata_layer.value == "X":
                    feature_val = active_adata[:, event.value].X
                else:
                    feature_val = active_adata[:, event.value].layers[widget.adata_layer.value]
            else:
                feature_val = active_adata.obs[event.value]
                
            try:
                quantiles=(0.01, 0.99)
                widget.lower_contrast_limit.value = np.nanquantile(feature_val, quantiles[0])
                widget.upper_contrast_limit.value = np.nanquantile(feature_val, quantiles[1])
                print("Updated contrast limits: ", np.quantile(feature_val, quantiles[0]), np.quantile(feature_val, quantiles[1]))
            except KeyError:
                # Don't update the limits if a feature name is entered that isn't in the dataframe
                pass
            
    @widget.adata_layer.changed.connect
    def update_rescaling_layer(event):
        print("VALUE CHANGED", event.value)
        
        if widget.adata_type.value == "cells":
            active_adata =  widget.sc_adata.value
        else:
            active_adata = widget.am_adata.value
            
            
        if event.value == '':
            widget.adata_layer.value = "X"
        

        if widget.feature_type.value == "ion":
            if widget.adata_layer.value == "X":
                feature_val = active_adata[:, widget.feature.value].X
            else:
                feature_val = active_adata[:, widget.feature.value].layers[widget.adata_layer.value]
        else:
            feature_val = active_adata.obs[widget.feature.value]
            
        feature_val = np.array(feature_val).astype(float)
        try:
            quantiles=(0.01, 0.99)
            widget.lower_contrast_limit.value = np.nanquantile(feature_val, quantiles[0])
            widget.upper_contrast_limit.value = np.nanquantile(feature_val, quantiles[1])
            print("Updated contrast limits: ", np.quantile(feature_val, quantiles[0]), np.quantile(feature_val, quantiles[1]))
        except KeyError:
            # Don't update the limits if a feature name is entered that isn't in the dataframe
            pass
        
        

@magic_factory(SpaceM_dir={'mode': 'd'},
               am_adata_path={'mode': 'r'},
               sc_adata_path={'mode': 'r'},
               feature_type = {"choices": ["ion", "obs"]},
                call_button="Apply Feature Colormap",
                layout='vertical',
                adata_type = {"choices": ["cells", "AMs"]},
                adata_layer = {"choices": ["X"]},
                lower_contrast_limit={"min": -100000000, "max": 100000000},
                upper_contrast_limit={"min": -100000000, "max": 100000000},
                feature = {"choices": [""]},
                label_column = {"choices": [""]}, 
                widget_init=_init,
                viewer={'visible': False, 'label': ' '},
                am_adata = {'visible': False, 'label': ' '},
                sc_adata = {'visible': False, 'label': ' '},
                cell_masks = {'visible': False, 'label': ' '},
                am_masks = {'visible': False, 'label': ' '}
        )
def spacem_feature_vis(viewer: 'napari.Viewer',
                       SpaceM_dir: pathlib.Path,
                       am_adata_path: pathlib.Path,
                       sc_adata_path: pathlib.Path,
                       adata_type="cells",
                        feature_type="ion",
                        feature='',
                        label_column='',
                        adata_layer="X",
                        Colormap=ColormapChoices.viridis,
                        lower_contrast_limit: float = 100,
                        upper_contrast_limit: float = 900,
                        am_adata = None,
                        sc_adata = None,
                        cell_masks = None,
                        am_masks = None
                        ):
    # Read SpaceM config
    ds_name = load_json(SpaceM_dir / "config.json")["input"]["dataset_name"]
    
    # Subset adata
    if adata_type == "cells":
        if ds_name not in sc_adata.obs.dataset:
            ds_name = ds_name[2:]
        active_adata =  sc_adata[sc_adata.obs.dataset == ds_name, :]
    else:
        if ds_name not in am_adata.obs.dataset:
            ds_name = ds_name[2:]
        active_adata = am_adata[am_adata.obs.dataset == ds_name, :]
    
    # Set layer properties
    layer_name = "%s_%s_%s"%(adata_type, feature, adata_layer)
    
    # Add/update layer and get its index
    print("FEATURE", feature)
    layer_names = [layer.name for layer in viewer.layers]
    if layer_name not in layer_names:
        if adata_type == "cells":
            viewer.add_labels(cell_masks, name=layer_name)
        else:
            viewer.add_labels(am_masks, name=layer_name)
    print("FEATURE", feature)
    label_layer = None
    for layer in viewer.layers:
        if layer.name == layer_name:
            label_layer = layer
    
    # Get feature values        
    if feature_type == "ion":
        if adata_layer == "X":
            feature_val = active_adata[:, feature].X
        else:
            feature_val = active_adata[:, feature].layers[adata_layer]
    else:
        feature_val = active_adata.obs[feature]
        
    print("feature_val", feature_val)
    
    # Do magic
    site_df = pd.DataFrame()
    site_df.loc[:, 'label'] = active_adata.obs[label_column].astype(int)
    site_df.loc[:, feature] = feature_val
    
    # Rescale feature between 0 & 1 to make a colormap
    site_df['feature_scaled'] = ((site_df[feature] - lower_contrast_limit) / (upper_contrast_limit - lower_contrast_limit))
    # Cap the measurement between 0 & 1
    site_df.loc[site_df['feature_scaled'] < 0, 'feature_scaled'] = 0
    site_df.loc[site_df['feature_scaled'] > 1, 'feature_scaled'] = 1

    colors = plt.cm.get_cmap(Colormap.value)(site_df['feature_scaled'])

    # Create an array where the index is the label value and the value is
    # the feature value
    print(site_df['label'].max())
    print("ALL LABELS")
    for i in site_df['label']:
        print(i)
    properties_array = np.zeros(site_df['label'].max() + 1)
    properties_array[site_df['label']] = site_df[feature]
    label_properties = {feature: np.round(properties_array, decimals=2)}

    colormap = dict(zip(site_df['label'], colors))
    label_layer.color = colormap
    label_layer.properties = label_properties
