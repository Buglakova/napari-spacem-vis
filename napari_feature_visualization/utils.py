# Util functions for plugins
from functools import lru_cache
import pandas as pd
import scanpy as sc
from enum import Enum
from pathlib import Path
from napari._qt.dialogs.qt_notification import NapariQtNotification
from napari._qt.qt_event_loop import _ipython_has_eventloop
import warnings
import json
from skimage import data, io
import napari.types

@lru_cache(maxsize=16)
def get_df(path):
    return pd.read_csv(path)


def get_anndata(path):
    return sc.read_h5ad(path)


def napari_warn(message):
    warnings.warn(message)
    if _ipython_has_eventloop():
        NapariQtNotification(message, 'WARNING').show()

def napari_info(message):
    print(message)
    if _ipython_has_eventloop():
        NapariQtNotification(message, 'INFO').show()


class ColormapChoices(Enum):
    viridis='viridis'
    plasma='plasma'
    inferno='inferno'
    magma='magma'
    cividis='cividis'
    Greys='Greys'
    Purples='Purples'
    Blues='Blues'
    Greens='Greens'
    Oranges='Oranges'
    Reds='Reds'
    YlOrBr='YlOrBr'
    YlOrRd='YlOrRd'
    OrRd='OrRd'
    PuRd='PuRd'
    RdPu='RdPu'
    BuPu='BuPu'
    GnBu='GnBu'
    PuBu='PuBu'
    YlGnBu='YlGnBu'
    PuBuGn='PuBuGn'
    BuGn='BuGn'
    YlGn='YlGn'
    PiYG='PiYG'
    PRGn='PRGn'
    BrBG='BrBG'
    PuOr='PuOr'
    RdGy='RdGy'
    RdBu='RdBu'
    RdYlBu='RdYlBu'
    RdYlGn='RdYlGn'
    Spectral='Spectral'
    coolwarm='coolwarm'
    bwr='bwr'
    seismic='seismic'
    turbo='turbo'
    jet='jet'
    
# SpaceM utils    

def load_json(json_path):
    with open(json_path) as json_file:
        json_dict = json.load(json_file)
    return json_dict


def add_spacem_images(dataset_path: Path, viewer, cell_segmentation="cell_segmentation_external"):
    ds_name = load_json(dataset_path / "config.json")["input"]["dataset_name"]

    state_path = dataset_path / "analysis"/ "pipeline_state.json"

    state = load_json(state_path)

    # Cell segmentation
    cell_masks = io.imread(dataset_path / state["results"][cell_segmentation]["cell_mask"])

    # Pre-maldi
    premaldi_paths = load_json(dataset_path / state["results"]["transformation"]['cropped_pre_maldi_channels_paths'])
    cropped_premaldi = {}
    for chan in premaldi_paths.keys():
        cropped_premaldi[chan] = io.imread(dataset_path / premaldi_paths[chan])

    # Post-maldi
    cropped_postmaldi = io.imread(dataset_path / "analysis" / "transformation" / "cropped_post_maldi_channels" / "img_t1_z1_c0.tif")

    # AM segmentation
    am_masks = io.imread(dataset_path / state["results"]["ion_image_registration"]['ablation_mark_mask'])

    # Overlap analysis
    overlap_masks = io.imread(dataset_path / state["results"]["overlap_analysis2"]['overlap_labels'])
    
    
    # Add to the viewer
    viewer.add_image(cropped_postmaldi, name="postmaldi_trans", opacity=0.5)
    layer_am_masks = viewer.add_labels(am_masks, name='am_segmentation')
    viewer.layers['am_segmentation'].contour = 1
    
    for chan in cropped_premaldi.keys():
        if "gfp" in chan.lower():
            colormap = "green"
            blending = "additive"
            opacity = 0.5
        elif "dapi" in chan.lower():
            colormap = "blue"
            blending = "additive"
            opacity = 0.5
        elif "trans" in chan.lower():
            colormap = "gray"
            blending = "translucent"
            opacity = 1
        else:
            colormap = "yellow"
            blending = "additive"
            opacity = 0.5
            
        viewer.add_image(cropped_premaldi[chan], name="premaldi_%s"%chan, opacity=opacity, blending=blending, colormap=colormap)
        
    viewer.add_labels(cell_masks, name='cell_segmentation')
    viewer.layers['cell_segmentation'].contour = 2

    
    return ds_name, cell_masks, cropped_premaldi, cropped_postmaldi, am_masks, overlap_masks


# def add_spacem_images(dataset_path: Path, cell_segmentation="cell_segmentation_external"):
#     ds_name = load_json(dataset_path / "config.json")["input"]["dataset_name"]

#     state_path = dataset_path / "analysis"/ "pipeline_state.json"

#     state = load_json(state_path)

#     # Cell segmentation
#     cell_masks = io.imread(dataset_path / state["results"][cell_segmentation]["cell_mask"])

#     # Pre-maldi
#     premaldi_paths = load_json(dataset_path / state["results"]["transformation"]['cropped_pre_maldi_channels_paths'])
#     cropped_premaldi = {}
#     for chan in premaldi_paths.keys():
#         cropped_premaldi[chan] = io.imread(dataset_path / premaldi_paths[chan])

#     # Post-maldi
#     postmaldi_paths = load_json(dataset_path / state["results"]["transformation"]['cropped_post_maldi_channels_paths'])
#     cropped_postmaldi = {}
#     for chan in postmaldi_paths.keys():
#         cropped_postmaldi[chan] = io.imread(dataset_path / postmaldi_paths[chan])

#     # AM segmentation
#     am_masks = io.imread(dataset_path / state["results"]["ion_image_registration"]['ablation_mark_mask'])

#     # Overlap analysis
#     overlap_masks = io.imread(dataset_path / state["results"]["overlap_analysis2"]['overlap_labels'])
    
    
#     # Add to the viewer

#     layer_list = []
    
   
#     layer_am_masks = (am_masks, {'name': "am_segmentation", "contour": 1}, "image")
#     layer_list.append(layer_am_masks)
#     return layer_list